const {
  YarnPluginWorkspacesDeps,
} = require("../packages/node/repository-tools");

module.exports = YarnPluginWorkspacesDeps("./packages");
